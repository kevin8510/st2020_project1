
let sorting = (array) => {
	array.sort(function(a,b){
		return parseInt(a)-parseInt(b);
	});
	// console.log(array);
    return array;
}

let compare = (a, b) => {
	var int_a = parseInt(a['PM2.5']);
	var int_b = parseInt(b['PM2.5']);
	int_a = +int_a || 0
	int_b = +int_b || 0
	if(int_a > int_b) return 1;
	if(int_a < int_b)  return -1;
	if(int_a == int_b)  return 0;
}

let average = (nums) => {
	var sum=0;
	var numbers=0;
	for(var i in nums){
		sum+=nums[i];
		numbers+=1;
	}

	return Math.round(sum/numbers*100)/100;
}


module.exports = {
    sorting,
    compare,
    average
}